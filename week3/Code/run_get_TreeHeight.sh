#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)
if (length(args)==0){
    Trees <- read.csv("../Data/trees.csv")
    Trees$Tree.Height.m <- Trees$Distance.m * tan(Trees$Angle.degrees * pi/100)
    write.table(Trees, file = "../Results/TreeHts.csv", sep=",", row.names=FALSE)}
if (length(args)==1) {
    Trees <- read.csv(args[1], header=TRUE)
    Trees$Tree.Height.m <- Trees[,2] * tan(Trees[,3] * pi/100)
    input <- basename(args[1])
    outname <- paste0(substr(input, start = 1, stop = nchar(input) - 4), "_treeheights.csv")
    write.table(Trees, outname, sep=",", row.names=FALSE)
}