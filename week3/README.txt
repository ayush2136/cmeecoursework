List of python scripts included for week3:

apply1.R: The function apply is used for applying the same function to rows/colums of a matrix

apply2.R: The function apply is used for applying the same function to rows/colums of a matrix

basic_io.R: The script reads the specified data and writes it as a table in a csv file.

boilerplate.R: Defines a function accepting "arguments" and "returning" a value

break.R: This script tests the break function to break out of loops.

browse.R: Runs a simulation of exponential growth. Returns a vector of length generations.

control.R: Script exemplifying control flow constructs in R 

DataWrang.R: The script modifies the dataset.

DataWrangTidy.R: The script uses dplyr and tidyr to modify the dataset

get_TreeHeight.R: This script takes a csv file name from the command line and outputs the result to a file, and includes the input file name in the output file name.

maps.R: The script does the following:
1.Loads the maps package
2.Loads the GPDD data
3.Creates a world map (use the map function, read its help, also google examples using maps
4.Superimposes on the map all the locations from which we have data in the GPDD dataframe

next.R: This script test the next command used to skip an iteration of a loop.

PP_Lattice.R: Reads the csv file EcolArchives-E089-51-D1.csv and outputs the desired graphs.

PP_Regress.R: Produces multiple regressions on a panel using data from EcolArchives-E089-51-D1.csv

preallocate.R: Tests the efficiency of running a loop with and without preallocation of a loop.

run_get_TreeHeight.sh: tests get_TreeHeight.R on Unix Terminal.

TAutoCorr.R: The script does the following:
1.Compute the appropriate correlation coefficient between successive years and store it (look at the help file for cor()
2.Repeat this calculation 10000 times by -- randomly permuting the time series, and then recalculating the correlation coefficient for each randomly permuted year sequence and storing it.
3.Calculates what fraction of the correlation coefficients from the previous step were greater than that from step 1 (this is your approximate p-value).

TreeHeight.R: This function calculates heights of trees given distance of each tree 
from its base and angle to its top, using  the trigonometric formula:height = distance * tan(radians) Loads trees.csv and calculates tree heights for all trees in the data and stores the data in a table as TreeHts.csv.

try.R: This script runs a simulation that involves sampling from a population with try.

Vectorize1.R: Vectorisation is attempted in this code script to optimise the code.

Vectorize2.py: This script attempts discrete time simulation of Ricker model in python.

Vectorize2.R: The script attempts the discrete time simulation of Ricker model in R

*All the scripts are stored in ~/Documents/CMEEcoursework/week3/Code
*external files that are required by certain scripts, are in ~/Documents/CMEEcoursework/week3/Data
*Output files of certain scripts, are in ~/Documents/CMEEcoursework/week3/Results


