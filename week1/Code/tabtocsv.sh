#!/bin/bash
echo "Creating a comma delimited version of $1..."
cat $1 | tr -s "\t" "," >> $1.csv
echo "Done!"
exit