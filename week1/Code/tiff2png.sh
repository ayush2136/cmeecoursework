#!/bin/bash
for f in *.tiff;
    do
        echo "Converting $f";
        convert "$f" "$(basename "$f" .tiff).png";
    done
