#!/bin/bash
wc -l E.coli.fasta 407228326.fasta 407228412.fasta > count.txt
head -3 count.txt 
tail -78103 E.coli.fasta 
tr -d '[:blank:]' < .E.coli.fasta | wc -c
grep -c ATGC E.coli.fasta
grep -c C E.coli.fasta < $1
grep -c G E.coli.fasta < $2
GC= 'expr $1 + $2' 
grep -c A E.coli.fasta < $4
grep -c T E.coli.fasta < $5
AT= 'expr $4 + $5'
Total= 'expr $GC + $AT'
echo $Total
