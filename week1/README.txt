File Locations:
The 3 specified fasta files in the format: .fasta, are in ~/Documents/CMEEcoursework/week1/Data
UnixPrac1.txt is in ~/Documents/CMEEcoursework/week1/Code
The shell scripts: biolerplate.sh, ConcatenateTwoFiles.sh, CountLines.sh, MyExampleScript.sh, tabtocsv.sh, UnixPrac1.txt, tiff2png and variables.sh, are in ~/Documents/CMEEcoursework/week1/Code

Functions of Bash Scripts:

boilerplate.sh- prints " This is a shell script! "

variables.sh- reads a string and prints it; reads 2 integers, adds them and prints the result.

MyExampleScript.sh- Concatenates and prints 2 strings, One defined as "Hello"and the other is the Username defined in the terminal

ConcatenateTwoFiles.sh- takes input of 2 files, concatenates the output and saves it in a third file.

tabtocsv.sh- replaces the 'space' with a ',' for the a .txt file and saves it as .csv file, Print "Done!".

CountLines.sh- count the line of a file and prints as a string the number of lines the file has.

CompileLatex.sh- Compiles the LaTex script as pdf document.

FirstExample.tex- Latex script to compile the desired document

tiff2png- changes the format of a image from .tiff to .png

Functions of the commands in the file: UnixPrac1.txt- 1. Count how many lines are in each file
						      2. Print everything starting from the second line for the E. coli genome
						      3. Count the sequence length of this genome
						      4. Count the matches of a particular sequence, "ATGC" in the genome of E. coli
						      5. Compute the AT/GC ratio.
