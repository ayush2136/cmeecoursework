
#starting population density of bacteria
N0<-c(0.1,0.1)
#starting concentration of resources 1 and 2
S0<-c(5,5)
#dilution rate of the chemostat
d<-0.01
#the amount of substrate a unit amount of enzyme can metabolise in a unit time
kcat<-0.1
#the Michaelis constant - concentration at which the reaction rate is 1/2 its maximum
Km<-0.5
#the conversion rate from substrate metabolised to density increase
c.conv<-1
#mutation rate of the Enzymes of 2 species to metabolise either of the resources
mu<-1.1
#Initial stress on the bacterial species
str=0.1
#effectivity of the antibiotics
e= 0.01
#penalty of producing the antibiotic.
da= 1
#Initial Susceptibility of the bacteria to the antibiotic
s=0.5
#Initial Antibiotic Concentration
A0= c(1,1)
##set up so can have different parameter for each species X substrate
##column = species, row = substrate
E=matrix(c(1,0,0,1),nrow=2,ncol=2)

##choose time steps and total time
time.step=0.1
times=seq(0,25,time.step)

##set initial values of state variables
A=A0
S=S0
N=N0

##store results
results=matrix(NA,nrow=length(times)+1,ncol=10)

	##store time 0 values
	results[1,1]=time.step
	results[1,2:3]=S
    results[1,4:5]=N
    results[1,6]=E[1,1]
    results[1,7]=E[1,2]
    results[1,8]=A[1]
    results[1,9]=A[2]
    results[1,10]=str

##loop through the times
for (i in (1:length(times))) {
#Increment in stress to either species; Maximum stress can be 1   
    str=str+.001
	if(str>1){
		str=1
	}
	

    P1= sum(N[1])/(sum(N[2])+1)
    P2= sum(N[2])/(sum(N[1])+1)	
    p1= ifelse(P1>1,1,P1)
    p2= ifelse(P2>1,1,P2)
    c1=A[1]*sum(N[2])*e
    c2=A[2]*sum(N[1])*e

#change in concentrations of Antibiotic produced by either of the species  
    A[1] = A[1] + e*p1*sum(N[1])-c1*A[1]*sum(N[2])-A[1]*d
    A[2] = A[2] + e*p2*sum(N[2])-c2*A[2]*sum(N[1])-A[2]*d

    if(A[1]<0){
        A[1]=0
    }
    if(A[2]<0){
        A[2]=0
    }
#change in substrate(resource) concentrations
	S[1] = S[1]-d*S[1]+d*S0[1]-sum((kcat*E[1,1]*S[1]/(Km+S[1]))*N[1])-sum((kcat*E[1,2]*S[1]/(Km+S[1]))*N[2])
	S[2] = S[2]-d*S[2]+sum((kcat*E[2,1]*S[2]/(Km+S[2]))*N[1])+sum((kcat*E[2,2]*S[2]/(Km+S[2]))*N[2])-sum((kcat*E[2,1]*S[2]/(Km+S[2]))*N[1])-sum((kcat*E[2,2]*S[2]/(Km+S[2]))*N[2])
#change in bacterial species densities
	N[1] = N[1]+(c.conv*(kcat*E[1,1]*S[1]/(Km+S[1]))*N[1])+(c.conv*(kcat*E[2,1]*S[2]/(Km+S[2]))*N[1])-(d*N[1]/str)-e*da*p1*N[1]-s*c1*(A[2])*N[1]
	N[2] = N[2]+(c.conv*(kcat*E[1,2]*S[1]/(Km+S[1]))*N[2])+(c.conv*(kcat*E[2,2]*S[2]/(Km+S[2]))*N[2])-(d*N[2]/str)-e*da*p2*N[2]-s*c2*(A[1])*N[2]
#change in enzymes for substrate 1
	E[1,1]= E[1,1]+mu*((c.conv*(kcat*S[1]/(Km+S[1])))-(c.conv*(kcat*S[2]/(Km+S[2]))))-(d*N[1]/str)-e*da*p1*N[1]-s*c1*(A[2])*N[1]
	E[1,2]= E[1,2]+mu*((c.conv*(kcat*S[1]/(Km+S[1])))-(c.conv*(kcat*S[2]/(Km+S[2]))))-(d*N[2]/str)-e*da*p2*N[2]-s*c2*(A[1])*N[2]
#corresponding opposite change in enzymes for substrate 2
	E[2,1]=1-E[1,1]
	E[2,2]=1-E[1,2]
	
##enforce boundary condition that total enzyme production is 1
	if (E[1,1]<0.01){
		E[1,1]=0.01
	}
	if (E[1,1]>1){
		E[1,1]=1
	}

	if (E[1,2]<0.01){
		E[1,2]=0.01
	}
	if (E[1,2]>1){
		E[1,2]=1
	}

	if (E[2,1]<0.01){
		E[2,1]=0.01
	}
	if (E[2,1]>1){
		E[2,1]=1
	}

	if (E[2,2]<0.01){
		E[2,2]=0.01
	}
	if (E[2,2]>1){
		E[2,2]=1
	}

	results[i+1,1]=times[i]
	results[i+1,2:3]=S
    results[i+1,4:5]=N
    results[i+1,6]=E[1,1]
    results[i+1,7]=E[1,2]
    results[i+1,8]=A[1]
    results[i+1,9]=A[2]
    results[i+1,10]=str
}



pdf("../Data/Resource.pdf", 11.7, 8.3) 
matplot(results[,1], results[,2:3], type="l", xlab="Time", 
        ylab="Density/Concentration",col=c("black","red"),
        lty=c(1,1,2,2))
legend(x=80,y=5,legend=c("Substrate1","Substrate2"), col=c("black","red"),lty=c(1,1,3,3))
graphics.off();

pdf("../Growth.pdf", 11.7, 8.3) 
matplot(results[,1], results[,4:5], type="l", xlab="Time", 
        ylab="Density/Concentration",col=c("black","red"),
        lty=c(1,1,2,2))
legend(x=80,y=5,legend=c("Species1","Species2"), col=c("black","red"),lty=c(1,1,3,3))
graphics.off();

pdf("../Data/Phenotype.pdf", 11.7, 8.3) 
matplot(results[,1], results[,6:7], type="l", xlab="Time", 
        ylab="Phenotype",col=c("black","red"),
        lty=c(1,1,2,2))
legend(x=80,y=0.4,legend=c("Species1","Species2"), col=c("black","red"),lty=c(1,1,3,3))
graphics.off();

pdf("../Data/Antibiotics.pdf", 11.7, 8.3) 
matplot(results[,1], results[,8:9], type="l", xlab="Time", 
        ylab="Density/Concentration",col=c("black","red"),
        lty=c(1,1,2,2))
legend(x=10,y=0.8,legend=c("Antibiotic1","Antibiotic2"), col=c("black","red"),lty=c(1,1,3,3))
graphics.off();

pdf("../Data/Antibiotics-Stress.pdf", 11.7, 8.3) 
matplot(results[,10], results[,8:9], type="l", xlab="Stress", 
        ylab="Density/Concentration",col=c("black","red"),
        lty=c(1,1,2,2))
legend(x=0.8,y=0.8,legend=c("Antibiotic1","Antibiotic2"), col=c("black","red"),lty=c(1,1,3,3))
graphics.off();

write.table(results, file = "../Data/data.csv", col.names= c("Times", "S1", "S2", "N1", "N2", "E1", "E2", "Anibiotic1", "Antibiotic2", "Stress"), row.names = FALSE, append = FALSE, sep = ",")

