echo "R is about to run"
R --vanilla < model4.R
echo "R has finished running"

pdflatex Mini_project.tex
pdflatex Mini_project.tex
pdflatex Mini_project.tex
pdflatex Mini_project.tex

rm *~
rm *.aux
rm *.dvi
rm *.log
rm *.nav
rm *.out
rm *.snm
rm *.toc
rm *.bbl
rm *.blg