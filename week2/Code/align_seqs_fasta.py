import sys
import csv

if len(sys.argv) > 2:
    f = open(sys.argv[1])
    csv_f = csv.reader(f)
    for row in csv_f:
        if row[0] == "seq1":
            seq1 = row[1]
    g = open(sys.argv[2])
    csv_g = csv.reader(g)
    for row in csv_g:
        if row[0] == "seq2":
            seq2 = row[1]
    print("args")
else:
    f = open('../Data/seq.csv','r')
    csv_f = csv.reader(f)
    for row in csv_f:
        if row[0] == "seq1":
            seq1 = row[1]
        elif row[0] == "seq2":
            seq2 = row[1]
        print("no args")

l1 = len(seq1)
l2 = len(seq2)
if l1 >= l2:
    s1 = seq1
    s2 = seq2
else:
    s1 = seq2
    s2 = seq1
    l1, l2 = l2, l1 # swap the two lengths
# function that computes a score
# by returning the number of matches 
# starting from arbitrary startpoint
def calculate_score(s1, s2, l1, l2, startpoint):
    # startpoint is the point at which we want to start
    matched = "" # contains string for alignement
    score = 0
    for i in range(l2):
        if (i + startpoint) < l1:
            # if its matching the character
            if s1[i + startpoint] == s2[i]:
                matched = matched + "*"
                score = score + 1
            else:
                matched = matched + "-"
 
    # build some formatted output
    print("." * startpoint + matched)        
    print("." * startpoint + s2)
    print(s1)
    print(score)
    print("")

    return score

calculate_score(s1, s2, l1, l2, 0)
calculate_score(s1, s2, l1, l2, 1)
calculate_score(s1, s2, l1, l2, 5)

# now try to find the best match (highest score)
my_best_align = None
my_best_score = -1

for i in range(l1):
    z = calculate_score(s1, s2, l1, l2, i)
    if z > my_best_score:
        my_best_align = "." * i + s2
        my_best_score = z

# Erase and rewrite the csv file

g = open('../Data/seq.csv','w')
csvwrite = csv.writer(g)
csvwrite.writerow(['seq1', seq1])
csvwrite.writerow(['seq2', seq2])
csvwrite.writerow(['Best Score', my_best_score])
csvwrite.writerow([my_best_align])
csvwrite.writerow([s1])
print(my_best_align)
print(s1)
print("Best score:", my_best_score)
