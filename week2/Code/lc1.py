#!/usr/bin/env python3

__author__ = 'Ayush Pathak (ap8418@ic.ac.uk)'
__version__ = '0.0.1'

import sys

birds = ( ('Passerculus sandwichensis','Savannah sparrow',18.7),
          ('Delichon urbica','House martin',19),
          ('Junco phaeonotus','Yellow-eyed junco',19.5),
          ('Junco hyemalis','Dark-eyed junco',19.6),
          ('Tachycineata bicolor','Tree swallow',20.2),
         )
def scientific_name(a):
    return[a[0] for a in birds]

def name(a):
    return[a[1] for a in birds]

def num(a):
    return[a[2] for a in birds]

def main(argv):
    print(scientific_name(birds))
    print(name(birds))
    print(num(birds))
    return 0

birds_lc = set ([a[0] for a in birds])
print(birds_lc)

birds_lc = set ([a[1] for a in birds])
print(birds_lc)

birds_lc = set ([a[2] for a in birds])
print(birds_lc)

if (__name__ == "__main__"):
    status = main(sys.argv)
    sys.exit(status)




#(1) Write three separate list comprehensions that create three different
# lists containing the latin names, common names and mean body masses for
# each species in birds, respectively. 

# (2) Now do the same using conventional loops (you can choose to do this 
# before 1 !). 
