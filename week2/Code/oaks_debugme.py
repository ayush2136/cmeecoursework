import csv
import sys
import doctest

#Define function
def is_an_oak(name):
    """ Returns True if name is starts with 'quercus'
     >>> is_an_oak("Quercuss robur")
    True

    >>> is_an_oak("Quercus cerris")
    True

    >>> is_an_oak("Fraxinus excelsior")
    True

    >>> is_an_oak("Pinus sylvestris")
    True

    >>> is_an_oak("Quercus petraea")
    True
     """
    return name.lower().startswith('quercus ')

def main(argv): 
    f = open('../Data/TestOaksData.csv','r')
    g = open('../Data/JustOaksData.csv','w')
    taxa = csv.reader(f)
    next(taxa, None)
    csvwrite = csv.writer(g)
    csvwrite.writerow(['Genus', ' Species'])
    for row in taxa:
        print(row)
        print ("The genus is: ") 
        print(row[0] + '\n')
        if is_an_oak(row[0] + " "):
            print('FOUND AN OAK!\n')
            csvwrite.writerow([row[0], row[1]])    

    return 0
    
if (__name__ == "__main__"):
    status = main(sys.argv)

doctest.testmod()