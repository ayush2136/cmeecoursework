# import ##
import sys # module to interface our program with the operating

## constants ##

## functions ##
def main(variable):
        """Main entry point of the program"""
        print(variable)
        print(variable[1])
        #return variable

if __name__ == "__main__":
        """Makes sure the "main" function is called from command line"""
        status = main(sys.argv)
        sys.exit(status)

