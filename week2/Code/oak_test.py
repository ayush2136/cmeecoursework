import csv
import sys


#Define function
def is_an_oak(name):
    return name.lower().startswith('quercus')

def main(argv): 
    f = open('../Data/TestOaksData.csv','r')
    g = open('../Data/JustOaksData.csv','w')
    taxa = csv.reader(f)
    csvwrite = csv.writer(g)
    #oaks = set()
    for row in taxa:
        print(row)
        print ("The genus is: ") 
        print(row[0] + '\n')
        if is_an_oak(row[0]):
                    #csvwrite.writerow(["Genus", "Species"])
                    csvwrite.writerow([row[0], row[1]]) 
                    print('FOUND AN OAK!\n')
                    


    return 0
    
#if (__name__ == "__main__"):
    status = main(sys.argv)
 

