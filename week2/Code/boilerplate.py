#!/usr/bin/env python3

__appname__ = '[App]'
__author__ = 'Ayush (pathak.ayush1996@gmail.com)'
__version__ = '0.0.1'
__license__ = "License for this  code/program"

## import ##
import sys # module to interface our program with the operating

## constants ##

## functions ##
def main(argv):
        """Main entry point of the program"""
        print('This is a boilerplate')
        return 0

if __name__ == "__main__":
        """Makes sure the "main" function is called from command line"""
        status = main(sys.argv)
        sys.exit(status)

