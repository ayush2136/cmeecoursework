List of python scripts included for week2:

align_seqs.py: The script takes the DNA sequences as an input from a single external file and saves the best alignment along with its corresponding score in a single text file (your choice of format and file type) to an appropriate location. 

align_seqs_fasta.py:Tis script takes anytwo fasta sequences (in separate files) to be aligned as input 

boilerplate.py: Defines a function to print "This is a boiler plate".

using_name.py: Informs the user if the programs is being imported or not.

control_flow.py: A collection of multiple functions using if else and for loops.

lc1.py: Writes three separate list comprehensions that create three different lists containing the latin names, common names and mean body masses for each species in birds, respectively. Does the same via usage of regular loops

oaks_debugme.py: Fixes the bugsofinitial script.

tuple.py: a script to print these on a separate line or output block by species 

basic_csv.py: Takes input for a csv file(testcsv.csv), and outputs the content via writing to another csv file

cfexercises1.py: Reiteration of a set of commands using for and while loops.

debugme.py: A script used to test the debugger in Visual Studio Code.       

lc2.py: (1) Use a list comprehension to create a list of month,rainfall tuples where the amount of rain was greater than 100 mm.
(2) Use a list comprehension to create a list of just month names where the amount of rain was less than 50 mm. 
(3) Does the same as the above via using regular loops 

oaks.py: A function is defined to find the Quercus taxa that are oak trees from a list of species.         

scope.py: Test whether a specific variable is inside or outside a defined function.

test_control_flow.py: Uses Doctests on control_flow.py to test the function used.

basic_io.py: Takes input from a text file (test.txt), and outputs the context via different looping conditions applied.

cfexercises2.py: A collection of multiple functions, if and while loops used within the functions.

dictionary.py: a script to populate a dictionary called taxa_dic derived from  taxa so that it maps order names to sets of taxa.   

loops.py: For and while loops used to specify different commands.  

oaks_debugme.py: Fixes the initial bug in the script

sysargv.py: Uses the command main(argv=sys.argv) to direct the interpreter to pass the argument variables to the main function.

Python scripts as part of the practicals of week2:
cfexercises2.py  lc2.py  lc1.py  tuple.py  dictionary.py  align_seqs.py  align_seqs_fasta.py  oaks_debugme.py  

*All the scripts are stored in ~/Documents/CMEEcoursework/week2/Code
*external files that are required by certain scripts, are in ~/Documents/CMEEcoursework/week2/Data

