List of python scripts included for week7:

blackbirds.py: Utilises Regex expressions to search for specified keywords in the data file and output the keywords to a csv file.  
  
DrawFW.py: Plots a food web network in python.

LV1.py: Simulates the Lotka-Volterra model as a continous time model in python.

LV2.py: Accepts arguments as parameters for the continous time Lotka-Volterra model in python. Simulates the model.

LV3.py: Simulates the Lotka-Volterra model as a discrete time model in python. 

profileme.py: The script was used to profile the code: locate the sections of the code where speed bottlenecks exist.  

profileme2.py: The script was used to profile the code: locate the sections of the code where speed bottlenecks exist. List comprehensions instead of loops were used.

regex.py: Uses regex expressions on various strings as an practise attempt to undertand the usage of regex expressions. 

run_fmr_R.py: Run the file fmr.R via python.

using_os.py: Uses the subprocess.os module to get a list of files and  directories in your ubuntu home directory               

Python scripts as part of the practicals of week2:
LV1.py  LV2.py  DrawFW.py  blackbirds.py  using_os.py  fmr.R  run_fmr_R.py  

*All the scripts are stored in ~/Documents/CMEEcoursework/week7/Code
*external files that are required by certain scripts, are in ~/Documents/CMEEcoursework/week7/Data
*Output files of certain scripts, are in ~/Documents/CMEEcoursework/week7/Results
