#!/usr/bin/env python3

import scipy as sc
import scipy.integrate as integrate
import matplotlib.pylab as p

def dCR_dt(pops, t=0):

    R = pops[0]
    C = pops[1]
    dRdt = R * (1.0 + r * (1.0 - R/K) - a * C)
    dCdt = C * (1.0 - z + e * a * R)
    return sc.array([dRdt, dCdt])

r = 1.
a = 0.1 
z = 1.5
e = 0.75
K = 50
t = sc.linspace(0, 15,  1000)

R0 = 10
C0 = 5 
RC0 = sc.array([R0, C0]) 

pops, infodict = integrate.odeint(dCR_dt, RC0, t, full_output=True)

print(pops)

print(infodict['message'])

f1 = p.figure()
p.plot(t, pops[:,0], 'g-', label='Resource density') # Plot
p.plot(t, pops[:,1]  , 'b-', label='Consumer density')
p.grid()
p.legend(loc='best')
p.xlabel('Time')
p.ylabel('Population density')
p.title('Consumer-Resource population dynamics ' + '(r = ' + str(r) + ' a = ' + str(a) + ' z = ' + str(z) + ' e = ' + str(e) + ')')
f1.savefig('../Results/LV_model.pdf')

f2 = p.figure()
p.plot(pops[:,1], pops[:,0], 'r-') 
p.grid()
p.legend(loc='best')
p.xlabel('Resource Density')
p.ylabel('Consumer density')
p.title('Consumer-Resource population dynamics' + '(r = ' + str(r) + ' a = ' + str(a) + ' z = ' + str(z) + ' e = ' + str(e) + ')')
f2.savefig('../Results/LV1_model.pdf')