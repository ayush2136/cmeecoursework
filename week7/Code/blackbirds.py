import re
import csv
# Read the file
f = open('../Data/blackbirds.txt', 'r')
text = f.read()
f.close()

# remove \t\n and put a space in:
text = text.replace('\t',' ')

# note that there are "strange characters" (these are accents and
# non-ascii symbols) because we don't care for them, first transform
# to ASCII:
text = text.decode('ascii', 'ignore') #will not work in python 3
Kingdom = re.findall(r"[K][i]\w+\s(!?[A-Z]\w+)", text)
Phylum = re.findall(r"[P][h]\w+\s(!?[A-Z]\w+)", text)
Species = re.findall(r"[S][p]\w+\s(!?[A-Z]\w+\s[a-z]\w+)", text)
li = list(zip(Kingdom, Phylum, Species))
with open("../Results/Taxonomic_Hierarchy.csv",'wb') as resultFile:
    wr = csv.writer(resultFile, dialect='excel')
    wr.writerow(li)
w = open('../Results/Taxonomic_Hierarchy.csv', 'r')
data = w.read()
print(data)
# Now extend this script so that it captures the Kingdom, 
# Phylum and Species name for each species and prints it out to screen neatly.

# Hint: you may want to use re.findall(my_reg, text)...
# Keep in mind that there are multiple ways to skin this cat! 
# Your solution may involve multiple regular expression calls (easier!), or a single one (harder!)
